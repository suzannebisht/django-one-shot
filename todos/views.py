from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_lists}
    return render(request, "todos/items.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_lists = form.save()
            return redirect("todo_list_detail", id=todo_lists.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_lists = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_lists)
        if form.is_valid():
            todo_lists = form.save()
            return redirect("todo_list_detail", id=todo_lists.id)
    else:
        form = TodoForm(instance=todo_lists)
    context = {
        "form": form,
        "todo_list": todo_lists,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_lists = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_lists.delete()
        return redirect("todo_list_list")
    context = {"todo_lists": todo_lists}
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            todo_list_id = todo_item.list.id
            return redirect("todo_list_detail", id=todo_list_id)
    else:
        form = TodoItemForm()

    todo_lists = TodoList.objects.all()
    context = {
        "form": form,
        "todo_lists": todo_lists,
    }
    return render(request, "todos/items_create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    todo_lists = TodoList.objects.all()
    context = {
        "form": form,
        "todo_lists": todo_lists,
        "todo_item": todo_item,  # Include todo_item in the context
    }
    return render(request, "todos/items_edit.html", context)
